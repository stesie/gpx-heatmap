#!/usr/bin/python3
from __future__ import print_function
import os
import sys

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

if not os.getenv("DEBUG"):
    import cgitb
    cgitb.enable()

import gheat

if os.getenv("DEBUG"):
    os.remove("/var/cache/"+os.getenv("REQUEST_URI"))

# REQUEST_URI is something like /stefan/classic/14/1077,696.png

fn = gheat.get_tile("/"+"/".join(os.getenv("REQUEST_URI").split("/")[-4:]))

if not os.path.exists(fn):
    print("Status: 404 Not Found")
    print("Content-type: text/plain")
    print("")
    print(fn)
    sys.exit(1)

if os.getenv("DEBUG"):
    print("Content-type: text/plain")
    print("")
    print(fn)
    sys.exit(0)

sys.stdout.buffer.write(bytes("Content-type: image/png\n\n", "utf-8"))
#sys.stdout.buffer.write(bytes(fn, "utf-8"))
#sys.stdout.flush()

with open(fn, 'rb') as fin:
    sys.stdout.buffer.write(fin.read())
