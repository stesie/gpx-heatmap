FROM ubuntu:jammy
MAINTAINER Stefan Siegl <stesie@brokenpipe.de>

STOPSIGNAL SIGHUP
RUN apt-get update && \
    apt-get -y install runit python3 python3-pymongo python3-pygame nginx fcgiwrap python3-future
CMD ["/etc/runit/2"]

ADD / /app
ADD /nginx/default /etc/nginx/sites-available/default

RUN mkdir /app/cache && chown www-data /app /app/cache && \
    ln -s /app/service/* /etc/runit/runsvdir/svmanaged/

EXPOSE 80
